/* Dooba SDK
 * KSZ9896 Gigabit Ethernet Switch Driver
 */

#ifndef	__KSZ9896_MONITOR_H
#define	__KSZ9896_MONITOR_H

// External Includes
#include <stdint.h>
#include <avr/io.h>

// Internal Includes
#include "ksz9896.h"

// Get current monitor output ("sniffer") port
extern uint8_t ksz9896_monitor_get_out_port(struct ksz9896 *k);

// Clear monitoring control
extern void ksz9896_monitor_clear(struct ksz9896 *k);

// Set monitor output ("sniffer") port
extern void ksz9896_monitor_set_out_port(struct ksz9896 *k, uint8_t port);

// Disable monitor output ("sniffer") port
extern void ksz9896_monitor_set_out_port_off(struct ksz9896 *k, uint8_t port);

// Configure monitor output ("sniffer") port
extern void ksz9896_monitor_set_out_port_s(struct ksz9896 *k, uint8_t port, uint8_t enable);

// Clear monitor output ("sniffer") port
extern void ksz9896_monitor_out_port_clear(struct ksz9896 *k);

// Sniff RX on port
extern void ksz9896_monitor_sniff_rx(struct ksz9896 *k, uint8_t port);

// Configure RX sniff on port
extern void ksz9896_monitor_set_sniff_rx(struct ksz9896 *k, uint8_t port, uint8_t enable);

// Sniff TX on port
extern void ksz9896_monitor_sniff_tx(struct ksz9896 *k, uint8_t port);

// Configure TX sniff on port
extern void ksz9896_monitor_set_sniff_tx(struct ksz9896 *k, uint8_t port, uint8_t enable);

// Clear sniff control on port
extern void ksz9896_monitor_sniff_clear(struct ksz9896 *k, uint8_t port);

// Clear sniff control on all ports
extern void ksz9896_monitor_sniff_clear_all(struct ksz9896 *k);

#endif
