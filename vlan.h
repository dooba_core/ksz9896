/* Dooba SDK
 * KSZ9896 Gigabit Ethernet Switch Driver
 */

#ifndef	__KSZ9896_VLAN_H
#define	__KSZ9896_VLAN_H

// External Includes
#include <stdint.h>
#include <avr/io.h>

// Internal Includes
#include "ksz9896.h"

// VLAN Count
#define	KSZ9896_VLAN_COUNT				4096

// Enable 802.1Q VLAN
extern void ksz9896_vlan_enable(struct ksz9896 *k);

// Disable 802.1Q VLAN
extern void ksz9896_vlan_disable(struct ksz9896 *k);

// Get 802.1Q VLAN Status
extern uint8_t ksz9896_vlan_status(struct ksz9896 *k);

// Configure dropping of frames with invalid VLAN ID (overridden by forwarding)
extern void ksz9896_vlan_drop_invalid_vid(struct ksz9896 *k, uint8_t drop);

// Get invalid VID frame drop
extern uint8_t ksz9896_vlan_get_drop_invalid_vid(struct ksz9896 *k);

// Configure forwarding of frames with unknown VLAN ID (overrides dropping)
extern void ksz9896_vlan_forward_invalid_vid(struct ksz9896 *k, uint8_t forward);

// Get forward invalid VID
extern uint8_t ksz9896_vlan_get_forward_invalid_vid(struct ksz9896 *k, uint8_t forward);

// Configure port forwarding of frames with unknown VLAN ID
extern void ksz9896_vlan_forward_invalid_vid_to_port(struct ksz9896 *k, uint8_t port_num, uint8_t forward);

// Get forward invalid VID to port
extern uint8_t ksz9896_vlan_get_forward_invalid_vid_to_port(struct ksz9896 *k, uint8_t port_num);

// Configure double tagging
extern void ksz9896_vlan_double_tag(struct ksz9896 *k, uint8_t enable);

// Get double tagging status
extern uint8_t ksz9896_vlan_get_double_tag(struct ksz9896 *k);

// Configure egress filtering
extern void ksz9896_vlan_egress_filter(struct ksz9896 *k, uint8_t dynamic_entry_filter, uint8_t static_entry_filter);

// Get egress filtering
extern void ksz9896_vlan_get_egress_filter(struct ksz9896 *k, uint8_t *dynamic_entry_filter, uint8_t *static_entry_filter);

// Configure trapping of multicast frames when double tagging is enabled
extern void ksz9896_vlan_trap_dt_mcast(struct ksz9896 *k, uint8_t trap);

// Get trap multicast when double tagging
extern uint8_t ksz9896_vlan_get_trap_dt_mcast(struct ksz9896 *k);

// Configure replacing of NULL VLAN IDs with port default
extern void ksz9896_vlan_replace_null_vid(struct ksz9896 *k, uint8_t replace);

// Get replace NULL VIDs with port default
extern uint8_t ksz9896_vlan_get_replace_null_vid(struct ksz9896 *k);

// Configure port default tag
extern void ksz9896_vlan_set_port_default(struct ksz9896 *k, uint8_t port_num, uint16_t vlan_id, uint8_t pcp, uint8_t dei);

// Get port default tag
extern void ksz9896_vlan_get_port_default(struct ksz9896 *k, uint8_t port_num, uint8_t *vlan_id, uint8_t *pcp, uint8_t *dei);

// Drop tagged frames on port
extern void ksz9896_vlan_set_port_drop_tagged(struct ksz9896 *k, uint8_t port_num, uint8_t drop);

// Get port drop tagged frames
extern uint8_t ksz9896_vlan_get_port_drop_tagged(struct ksz9896 *k, uint8_t port_num);

// Drop untagged frames on port
extern void ksz9896_vlan_set_port_drop_untagged(struct ksz9896 *k, uint8_t port_num, uint8_t drop);

// Get port drop untagged frames
extern uint8_t ksz9896_vlan_get_port_drop_untagged(struct ksz9896 *k, uint8_t port_num);

// Replace tags on port egress with port default
extern void ksz9896_vlan_set_port_pvid_replacement(struct ksz9896 *k, uint8_t port_num, uint8_t replace);

// Get port egress PVID replacement
extern uint8_t ksz9896_vlan_get_port_pvid_replacement(struct ksz9896 *k, uint8_t port_num);

// Configure port NULL VID lookup
extern void ksz9896_vlan_set_port_null_vid_lookup(struct ksz9896 *k, uint8_t port_num, uint8_t enable);

// Get port NULL VID lookup
extern uint8_t ksz9896_vlan_get_port_null_vid_lookup(struct ksz9896 *k, uint8_t port_num);

// Configure port ingress VLAN filtering
extern void ksz9896_vlan_set_port_ingress_filter(struct ksz9896 *k, uint8_t port_num, uint8_t enable);

// Get port ingress VLAN filtering
extern uint8_t ksz9896_vlan_get_port_ingress_filter(struct ksz9896 *k, uint8_t port_num);

// Discard frames where VLAN ID does not match the port default
extern void ksz9896_vlan_set_port_ingress_pvid_filter(struct ksz9896 *k, uint8_t port_num, uint8_t enable);

// Get port ingress PVID filtering
extern uint8_t ksz9896_vlan_get_port_ingress_pvid_filter(struct ksz9896 *k, uint8_t port_num);

// Read entry from the VLAN table
extern void ksz9896_vlan_read_vtable(struct ksz9896 *k, uint16_t id, uint8_t *valid, uint8_t *forward, uint8_t *priority, uint8_t *mstp_index, uint8_t *fid, uint8_t *port_untag, uint8_t *port_forward);

// Write entry to the VLAN table
extern void ksz9896_vlan_write_vtable(struct ksz9896 *k, uint16_t id, uint8_t valid, uint8_t forward, uint8_t priority, uint8_t mstp_index, uint8_t fid, uint8_t port_untag, uint8_t port_forward);

// Clear VLAN table
extern void ksz9896_vlan_clear_vtable(struct ksz9896 *k, uint16_t id, uint8_t valid, uint8_t forward, uint8_t priority, uint8_t mstp_index, uint8_t fid, uint8_t port_untag, uint8_t port_forward);

#endif
