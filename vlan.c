/* Dooba SDK
 * KSZ9896 Gigabit Ethernet Switch Driver
 */

// External Includes
#include <util/delay.h>

// Internal Includes
#include "regs.h"
#include "vlan.h"

// Enable 802.1Q VLAN
void ksz9896_vlan_enable(struct ksz9896 *k)
{
	uint8_t r;

	// Enable VLAN
	r = ksz9896_read_reg(k, KSZ9896_REG_SWITCH_LUE_CTL_0);
	ksz9896_write_reg(k, KSZ9896_REG_SWITCH_LUE_CTL_0, r | KSZ9896_REG_SWITCH_LUE_CTL_0_VLANEN);
}

// Disable 802.1Q VLAN
void ksz9896_vlan_disable(struct ksz9896 *k)
{
	uint8_t r;

	// Disable VLAN
	r = ksz9896_read_reg(k, KSZ9896_REG_SWITCH_LUE_CTL_0);
	ksz9896_write_reg(k, KSZ9896_REG_SWITCH_LUE_CTL_0, r & ~KSZ9896_REG_SWITCH_LUE_CTL_0_VLANEN);
}

// Get 802.1Q VLAN Status
uint8_t ksz9896_vlan_status(struct ksz9896 *k)
{
	uint8_t r;

	// Get status
	r = ksz9896_read_reg(k, KSZ9896_REG_SWITCH_LUE_CTL_0);
	return (r & KSZ9896_REG_SWITCH_LUE_CTL_0_VLANEN) != 0;
}

// Configure dropping of frames with invalid VLAN ID (overridden by forwarding)
void ksz9896_vlan_drop_invalid_vid(struct ksz9896 *k, uint8_t drop)
{
	uint8_t r;

	// Configure
	r = ksz9896_read_reg(k, KSZ9896_REG_SWITCH_LUE_CTL_0);
	if(drop)													{ r = r | KSZ9896_REG_SWITCH_LUE_CTL_0_DROPINVALIDVID; }
	else														{ r = r & ~KSZ9896_REG_SWITCH_LUE_CTL_0_DROPINVALIDVID; }
	ksz9896_write_reg(k, KSZ9896_REG_SWITCH_LUE_CTL_0, r);
}

// Get invalid VID frame drop
uint8_t ksz9896_vlan_get_drop_invalid_vid(struct ksz9896 *k)
{
	uint8_t r;

	// Get status
	r = ksz9896_read_reg(k, KSZ9896_REG_SWITCH_LUE_CTL_0);
	return (r & KSZ9896_REG_SWITCH_LUE_CTL_0_DROPINVALIDVID) != 0;
}

// Configure forwarding of frames with unknown VLAN ID (overrides dropping)
void ksz9896_vlan_forward_invalid_vid(struct ksz9896 *k, uint8_t forward)
{
	uint32_t r;

	// Configure
	r = ksz9896_read_reg_l(k, KSZ9896_REG_UNK_VLAN_ID_CTL);
	if(forward)													{ r = r | KSZ9896_REG_UNK_VLAN_ID_CTL_FWDENABLE; }
	else														{ r = r & ~KSZ9896_REG_UNK_VLAN_ID_CTL_FWDENABLE; }
	ksz9896_write_reg_l(k, KSZ9896_REG_UNK_VLAN_ID_CTL, r);
}

// Get forward invalid VID
uint8_t ksz9896_vlan_get_forward_invalid_vid(struct ksz9896 *k, uint8_t forward)
{
	uint32_t r;

	// Get status
	r = ksz9896_read_reg_l(k, KSZ9896_REG_UNK_VLAN_ID_CTL);
	return (r & KSZ9896_REG_UNK_VLAN_ID_CTL_FWDENABLE) != 0;
}

// Configure port forwarding of frames with unknown VLAN ID
void ksz9896_vlan_forward_invalid_vid_to_port(struct ksz9896 *k, uint8_t port_num, uint8_t forward)
{
	uint32_t r;

	// Configure
	r = ksz9896_read_reg_l(k, KSZ9896_REG_UNK_VLAN_ID_CTL);
	if(forward)													{ r = r | (((uint32_t)1) << port_num); }
	else														{ r = r & ~(((uint32_t)1) << port_num); }
	ksz9896_write_reg_l(k, KSZ9896_REG_UNK_VLAN_ID_CTL, r);
}

// Get forward invalid VID to port
uint8_t ksz9896_vlan_get_forward_invalid_vid_to_port(struct ksz9896 *k, uint8_t port_num)
{
	uint32_t r;

	// Get status
	r = ksz9896_read_reg_l(k, KSZ9896_REG_UNK_VLAN_ID_CTL);
	return (r & (((uint32_t)1) << port_num)) != 0;
}

// Configure double tagging
void ksz9896_vlan_double_tag(struct ksz9896 *k, uint8_t enable)
{
	uint8_t r;

	// Configure
	r = ksz9896_read_reg(k, KSZ9896_REG_SWITCH_OP);
	if(enable)													{ r = r | KSZ9896_REG_SWITCH_OP_DOUBLETAG; }
	else														{ r = r & ~KSZ9896_REG_SWITCH_OP_DOUBLETAG; }
	ksz9896_write_reg(k, KSZ9896_REG_SWITCH_OP, r);
}

// Get double tagging status
uint8_t ksz9896_vlan_get_double_tag(struct ksz9896 *k)
{
	uint8_t r;

	// Get status
	r = ksz9896_read_reg(k, KSZ9896_REG_SWITCH_OP);
	return (r & KSZ9896_REG_SWITCH_OP_DOUBLETAG) != 0;
}

// Configure egress filtering
void ksz9896_vlan_egress_filter(struct ksz9896 *k, uint8_t dynamic_entry_filter, uint8_t static_entry_filter)
{
	uint8_t r;

	// Configure
	r = ksz9896_read_reg(k, KSZ9896_REG_SWITCH_LUE_CTL_2);
	if(dynamic_entry_filter)									{ r = r | KSZ9896_REG_SWITCH_LUE_CTL_2_STA_EGRESS_FILTER; }
	else														{ r = r & ~KSZ9896_REG_SWITCH_LUE_CTL_2_STA_EGRESS_FILTER; }
	if(static_entry_filter)										{ r = r | KSZ9896_REG_SWITCH_LUE_CTL_2_STA_EGRESS_FILTER; }
	else														{ r = r & ~KSZ9896_REG_SWITCH_LUE_CTL_2_STA_EGRESS_FILTER; }
	ksz9896_write_reg(k, KSZ9896_REG_SWITCH_LUE_CTL_2, r);
}

// Get egress filtering
void ksz9896_vlan_get_egress_filter(struct ksz9896 *k, uint8_t *dynamic_entry_filter, uint8_t *static_entry_filter)
{
	uint8_t r;

	// Get status
	r = ksz9896_read_reg(k, KSZ9896_REG_SWITCH_LUE_CTL_2);
	if(dynamic_entry_filter)									{ *dynamic_entry_filter = ((r & KSZ9896_REG_SWITCH_LUE_CTL_2_DYN_EGRESS_FILTER) != 0); }
	if(static_entry_filter)										{ *static_entry_filter = ((r & KSZ9896_REG_SWITCH_LUE_CTL_2_STA_EGRESS_FILTER) != 0); }
}

// Configure trapping of multicast frames when double tagging is enabled
void ksz9896_vlan_trap_dt_mcast(struct ksz9896 *k, uint8_t trap)
{
	uint8_t r;

	// Configure
	r = ksz9896_read_reg(k, KSZ9896_REG_SWITCH_LUE_CTL_2);
	if(trap)													{ r = r | KSZ9896_REG_SWITCH_LUE_CTL_2_DT_MCAST_TRAP; }
	else														{ r = r & ~KSZ9896_REG_SWITCH_LUE_CTL_2_DT_MCAST_TRAP; }
	ksz9896_write_reg(k, KSZ9896_REG_SWITCH_LUE_CTL_2, r);
}

// Get trap multicast when double tagging
uint8_t ksz9896_vlan_get_trap_dt_mcast(struct ksz9896 *k)
{
	uint8_t r;

	// Get status
	r = ksz9896_read_reg(k, KSZ9896_REG_SWITCH_LUE_CTL_2);
	return (r & KSZ9896_REG_SWITCH_LUE_CTL_2_DT_MCAST_TRAP) != 0;
}

// Configure replacing of NULL VLAN IDs with port default
void ksz9896_vlan_replace_null_vid(struct ksz9896 *k, uint8_t replace)
{
	uint8_t r;

	// Configure
	r = ksz9896_read_reg(k, KSZ9896_REG_SWITCH_MAC_CTL_2);
	if(replace)													{ r = r | KSZ9896_REG_SWITCH_MAC_CTL_2_NULLVIDREPLACE; }
	else														{ r = r & ~KSZ9896_REG_SWITCH_MAC_CTL_2_NULLVIDREPLACE; }
	ksz9896_write_reg(k, KSZ9896_REG_SWITCH_MAC_CTL_2, r);
}

// Get replace NULL VIDs with port default
uint8_t ksz9896_vlan_get_replace_null_vid(struct ksz9896 *k)
{
	uint8_t r;

	// Get status
	r = ksz9896_read_reg(k, KSZ9896_REG_SWITCH_MAC_CTL_2);
	return (r & KSZ9896_REG_SWITCH_MAC_CTL_2_NULLVIDREPLACE) != 0;
}

// Configure port default tag
void ksz9896_vlan_set_port_default(struct ksz9896 *k, uint8_t port_num, uint16_t vlan_id, uint8_t pcp, uint8_t dei)
{
	uint8_t dflt0;
	uint8_t dflt1;

	// Prepare
	dflt0 = ((vlan_id >> 8) & 0x0f) | (dei ? KSZ9896_REG_PORT_DFLT_TAG_0_DEI : 0) | ((pcp & KSZ9896_REG_PORT_DFLT_TAG_0_PCP_MASK) << KSZ9896_REG_PORT_DFLT_TAG_0_PCP_OFF);
	dflt1 = vlan_id & 0x00ff;

	// Configure
	ksz9896_write_reg(k, KSZ9896_REG_PORT_DFLT_TAG_0(port_num), dflt0);
	ksz9896_write_reg(k, KSZ9896_REG_PORT_DFLT_TAG_1(port_num), dflt1);
}

// Get port default tag
void ksz9896_vlan_get_port_default(struct ksz9896 *k, uint8_t port_num, uint8_t *vlan_id, uint8_t *pcp, uint8_t *dei)
{
	uint8_t dflt0;
	uint8_t dflt1;

	// Get status
	dflt0 = ksz9896_read_reg(k, KSZ9896_REG_PORT_DFLT_TAG_0(port_num));
	dflt1 = ksz9896_read_reg(k, KSZ9896_REG_PORT_DFLT_TAG_1(port_num));
	if(vlan_id)													{ *vlan_id = ((uint16_t)dflt1) | (((uint16_t)(dflt0 & 0x0f)) << 8); }
	if(pcp)														{ *pcp = (dflt0 >> KSZ9896_REG_PORT_DFLT_TAG_0_PCP_OFF) & KSZ9896_REG_PORT_DFLT_TAG_0_PCP_MASK; }
	if(dei)														{ *dei = (dflt0 & KSZ9896_REG_PORT_DFLT_TAG_0_DEI) != 0; }
}

// Drop tagged frames on port
void ksz9896_vlan_set_port_drop_tagged(struct ksz9896 *k, uint8_t port_num, uint8_t drop)
{
	uint8_t r;

	// Configure
	r = ksz9896_read_reg(k, KSZ9896_REG_PORT_INGRESS_MAC_CTL(port_num));
	if(drop)													{ r = r | KSZ9896_REG_PORT_INGRESS_MAC_CTL_DROP_TAGGED; }
	else														{ r = r & ~KSZ9896_REG_PORT_INGRESS_MAC_CTL_DROP_TAGGED; }
	ksz9896_write_reg(k, KSZ9896_REG_PORT_INGRESS_MAC_CTL(port_num), r);
}

// Get port drop tagged frames
uint8_t ksz9896_vlan_get_port_drop_tagged(struct ksz9896 *k, uint8_t port_num)
{
	uint8_t r;

	// Get status
	r = ksz9896_read_reg(k, KSZ9896_REG_PORT_INGRESS_MAC_CTL(port_num));
	return (r & KSZ9896_REG_PORT_INGRESS_MAC_CTL_DROP_TAGGED) != 0;
}

// Drop untagged frames on port
void ksz9896_vlan_set_port_drop_untagged(struct ksz9896 *k, uint8_t port_num, uint8_t drop)
{
	uint8_t r;

	// Configure
	r = ksz9896_read_reg(k, KSZ9896_REG_PORT_INGRESS_MAC_CTL(port_num));
	if(drop)													{ r = r | KSZ9896_REG_PORT_INGRESS_MAC_CTL_DROP_UNTAGGED; }
	else														{ r = r & ~KSZ9896_REG_PORT_INGRESS_MAC_CTL_DROP_UNTAGGED; }
	ksz9896_write_reg(k, KSZ9896_REG_PORT_INGRESS_MAC_CTL(port_num), r);
}

// Get port drop untagged frames
uint8_t ksz9896_vlan_get_port_drop_untagged(struct ksz9896 *k, uint8_t port_num)
{
	uint8_t r;

	// Get status
	r = ksz9896_read_reg(k, KSZ9896_REG_PORT_INGRESS_MAC_CTL(port_num));
	return (r & KSZ9896_REG_PORT_INGRESS_MAC_CTL_DROP_UNTAGGED) != 0;
}

// Replace tags on port egress with port default
void ksz9896_vlan_set_port_pvid_replacement(struct ksz9896 *k, uint8_t port_num, uint8_t replace)
{
	uint32_t r;

	// Configure
	r = ksz9896_read_reg_l(k, KSZ9896_REG_PORT_TX_QUEUE_PVID(port_num));
	if(replace)													{ r = r | KSZ9896_REG_PORT_TX_QUEUE_PVID_REPLACE; }
	else														{ r = r & ~KSZ9896_REG_PORT_TX_QUEUE_PVID_REPLACE; }
	ksz9896_write_reg_l(k, KSZ9896_REG_PORT_TX_QUEUE_PVID(port_num), r);
}

// Get port egress PVID replacement
uint8_t ksz9896_vlan_get_port_pvid_replacement(struct ksz9896 *k, uint8_t port_num)
{
	uint32_t r;

	// Get status
	r = ksz9896_read_reg_l(k, KSZ9896_REG_PORT_TX_QUEUE_PVID(port_num));
	return (r & KSZ9896_REG_PORT_TX_QUEUE_PVID_REPLACE) != 0;
}

// Configure port NULL VID lookup
void ksz9896_vlan_set_port_null_vid_lookup(struct ksz9896 *k, uint8_t port_num, uint8_t enable)
{
	uint8_t r;

	// Configure
	r = ksz9896_read_reg(k, KSZ9896_REG_PORT_CTL_2(port_num));
	if(enable)													{ r = r | KSZ9896_REG_PORT_CTL_2_NULLVIDLOOKUP; }
	else														{ r = r & ~KSZ9896_REG_PORT_CTL_2_NULLVIDLOOKUP; }
	ksz9896_write_reg(k, KSZ9896_REG_PORT_CTL_2(port_num), r);
}

// Get port NULL VID lookup
uint8_t ksz9896_vlan_get_port_null_vid_lookup(struct ksz9896 *k, uint8_t port_num)
{
	uint8_t r;

	// Get status
	r = ksz9896_read_reg(k, KSZ9896_REG_PORT_CTL_2(port_num));
	return (r & KSZ9896_REG_PORT_CTL_2_NULLVIDLOOKUP) != 0;
}

// Configure port ingress VLAN filtering
void ksz9896_vlan_set_port_ingress_filter(struct ksz9896 *k, uint8_t port_num, uint8_t enable)
{
	uint8_t r;

	// Configure
	r = ksz9896_read_reg(k, KSZ9896_REG_PORT_CTL_2(port_num));
	if(enable)													{ r = r | KSZ9896_REG_PORT_CTL_2_INGRESS_VLAN_FILTER; }
	else														{ r = r & ~KSZ9896_REG_PORT_CTL_2_INGRESS_VLAN_FILTER; }
	ksz9896_write_reg(k, KSZ9896_REG_PORT_CTL_2(port_num), r);
}

// Get port ingress VLAN filtering
uint8_t ksz9896_vlan_get_port_ingress_filter(struct ksz9896 *k, uint8_t port_num)
{
	uint8_t r;

	// Get status
	r = ksz9896_read_reg(k, KSZ9896_REG_PORT_CTL_2(port_num));
	return (r & KSZ9896_REG_PORT_CTL_2_INGRESS_VLAN_FILTER) != 0;
}

// Discard frames where VLAN ID does not match the port default
void ksz9896_vlan_set_port_ingress_pvid_filter(struct ksz9896 *k, uint8_t port_num, uint8_t enable)
{
	uint8_t r;

	// Configure
	r = ksz9896_read_reg(k, KSZ9896_REG_PORT_CTL_2(port_num));
	if(enable)													{ r = r | KSZ9896_REG_PORT_CTL_2_DISCARD_NONPVID; }
	else														{ r = r & ~KSZ9896_REG_PORT_CTL_2_DISCARD_NONPVID; }
	ksz9896_write_reg(k, KSZ9896_REG_PORT_CTL_2(port_num), r);
}

// Get port ingress PVID filtering
uint8_t ksz9896_vlan_get_port_ingress_pvid_filter(struct ksz9896 *k, uint8_t port_num)
{
	uint8_t r;

	// Get status
	r = ksz9896_read_reg(k, KSZ9896_REG_PORT_CTL_2(port_num));
	return (r & KSZ9896_REG_PORT_CTL_2_DISCARD_NONPVID) != 0;
}

// Read entry from the VLAN table
void ksz9896_vlan_read_vtable(struct ksz9896 *k, uint16_t id, uint8_t *valid, uint8_t *forward, uint8_t *priority, uint8_t *mstp_index, uint8_t *fid, uint8_t *port_untag, uint8_t *port_forward)
{
	uint8_t r;
	uint32_t vte0;
	uint32_t vte1;
	uint32_t vte2;

	// Setup
	ksz9896_write_reg_s(k, KSZ9896_REG_VLAN_TABLE_INDEX, id);
	ksz9896_write_reg(k, KSZ9896_REG_VLAN_TABLE_ACCESS_CTL, KSZ9896_REG_VLAN_TABLE_ACCESS_CTL_ACT_READ | KSZ9896_REG_VLAN_TABLE_ACCESS_CTL_START);

	// Wait for operation to complete
	r = ksz9896_read_reg(k, KSZ9896_REG_VLAN_TABLE_ACCESS_CTL);
	while(r & KSZ9896_REG_VLAN_TABLE_ACCESS_CTL_START)					{ r = ksz9896_read_reg(k, KSZ9896_REG_VLAN_TABLE_ACCESS_CTL); }

	// Get info
	vte0 = ksz9896_read_reg_l(k, KSZ9896_REG_VLAN_TABLE_ENTRY_0);
	vte1 = ksz9896_read_reg_l(k, KSZ9896_REG_VLAN_TABLE_ENTRY_1);
	vte2 = ksz9896_read_reg_l(k, KSZ9896_REG_VLAN_TABLE_ENTRY_2);
	if(valid)															{ *valid = (vte0 & KSZ9896_REG_VLAN_TABLE_ENTRY_0_VALID) != 0; }
	if(forward)															{ *forward = (vte0 & KSZ9896_REG_VLAN_TABLE_ENTRY_0_FORWARD) != 0; }
	if(priority)														{ *priority = (vte0 >> KSZ9896_REG_VLAN_TABLE_ENTRY_0_PRIO_OFF) & KSZ9896_REG_VLAN_TABLE_ENTRY_0_PRIO_MASK; }
	if(mstp_index)														{ *mstp_index = (vte0 >> KSZ9896_REG_VLAN_TABLE_ENTRY_0_MSTPIDX_OFF) & KSZ9896_REG_VLAN_TABLE_ENTRY_0_MSTPIDX_MASK; }
	if(fid)																{ *fid = vte0 & KSZ9896_REG_VLAN_TABLE_ENTRY_0_FID_MASK; }
	if(port_untag)														{ *port_untag = vte1 & KSZ9896_REG_VLAN_TABLE_ENTRY_1_PORT_UNTAG_MASK; }
	if(port_forward)													{ *port_forward = vte2 & KSZ9896_REG_VLAN_TABLE_ENTRY_2_PORT_FORWARD_MASK; }
}

// Write entry to the VLAN table
void ksz9896_vlan_write_vtable(struct ksz9896 *k, uint16_t id, uint8_t valid, uint8_t forward, uint8_t priority, uint8_t mstp_index, uint8_t fid, uint8_t port_untag, uint8_t port_forward)
{
	uint8_t r;
	uint32_t vte0;
	uint32_t vte1;
	uint32_t vte2;

	// Prepare entry
	vte0 = (valid ? KSZ9896_REG_VLAN_TABLE_ENTRY_0_VALID : 0) \
		| (forward ? KSZ9896_REG_VLAN_TABLE_ENTRY_0_FORWARD : 0) \
		| ((((uint32_t)priority) & KSZ9896_REG_VLAN_TABLE_ENTRY_0_PRIO_MASK) << KSZ9896_REG_VLAN_TABLE_ENTRY_0_PRIO_OFF) \
		| ((((uint32_t)mstp_index) & KSZ9896_REG_VLAN_TABLE_ENTRY_0_MSTPIDX_MASK) << KSZ9896_REG_VLAN_TABLE_ENTRY_0_MSTPIDX_OFF) \
		| ((((uint32_t)fid) & KSZ9896_REG_VLAN_TABLE_ENTRY_0_FID_MASK));
	vte1 = port_untag & KSZ9896_REG_VLAN_TABLE_ENTRY_1_PORT_UNTAG_MASK;
	vte2 = port_forward & KSZ9896_REG_VLAN_TABLE_ENTRY_2_PORT_FORWARD_MASK;

	// Setup
	ksz9896_write_reg_l(k, KSZ9896_REG_VLAN_TABLE_ENTRY_0, vte0);
	ksz9896_write_reg_l(k, KSZ9896_REG_VLAN_TABLE_ENTRY_1, vte1);
	ksz9896_write_reg_l(k, KSZ9896_REG_VLAN_TABLE_ENTRY_2, vte2);
	ksz9896_write_reg_s(k, KSZ9896_REG_VLAN_TABLE_INDEX, id);
	ksz9896_write_reg(k, KSZ9896_REG_VLAN_TABLE_ACCESS_CTL, KSZ9896_REG_VLAN_TABLE_ACCESS_CTL_ACT_WRITE | KSZ9896_REG_VLAN_TABLE_ACCESS_CTL_START);

	// Wait for operation to complete
	r = ksz9896_read_reg(k, KSZ9896_REG_VLAN_TABLE_ACCESS_CTL);
	while(r & KSZ9896_REG_VLAN_TABLE_ACCESS_CTL_START)					{ r = ksz9896_read_reg(k, KSZ9896_REG_VLAN_TABLE_ACCESS_CTL); }
}

// Clear VLAN table
void ksz9896_vlan_clear_vtable(struct ksz9896 *k, uint16_t id, uint8_t valid, uint8_t forward, uint8_t priority, uint8_t mstp_index, uint8_t fid, uint8_t port_untag, uint8_t port_forward)
{
	uint8_t r;
	uint32_t vte0;
	uint32_t vte1;
	uint32_t vte2;

	// Prepare entry
	vte0 = (valid ? KSZ9896_REG_VLAN_TABLE_ENTRY_0_VALID : 0) \
		| (forward ? KSZ9896_REG_VLAN_TABLE_ENTRY_0_FORWARD : 0) \
		| ((((uint32_t)priority) & KSZ9896_REG_VLAN_TABLE_ENTRY_0_PRIO_MASK) << KSZ9896_REG_VLAN_TABLE_ENTRY_0_PRIO_OFF) \
		| ((((uint32_t)mstp_index) & KSZ9896_REG_VLAN_TABLE_ENTRY_0_MSTPIDX_MASK) << KSZ9896_REG_VLAN_TABLE_ENTRY_0_MSTPIDX_OFF) \
		| ((((uint32_t)fid) & KSZ9896_REG_VLAN_TABLE_ENTRY_0_FID_MASK));
	vte1 = port_untag & KSZ9896_REG_VLAN_TABLE_ENTRY_1_PORT_UNTAG_MASK;
	vte2 = port_forward & KSZ9896_REG_VLAN_TABLE_ENTRY_2_PORT_FORWARD_MASK;

	// Setup
	ksz9896_write_reg_l(k, KSZ9896_REG_VLAN_TABLE_ENTRY_0, vte0);
	ksz9896_write_reg_l(k, KSZ9896_REG_VLAN_TABLE_ENTRY_1, vte1);
	ksz9896_write_reg_l(k, KSZ9896_REG_VLAN_TABLE_ENTRY_2, vte2);
	ksz9896_write_reg_s(k, KSZ9896_REG_VLAN_TABLE_INDEX, id);
	ksz9896_write_reg(k, KSZ9896_REG_VLAN_TABLE_ACCESS_CTL, KSZ9896_REG_VLAN_TABLE_ACCESS_CTL_ACT_CLEAR | KSZ9896_REG_VLAN_TABLE_ACCESS_CTL_START);

	// Wait for operation to complete
	r = ksz9896_read_reg(k, KSZ9896_REG_VLAN_TABLE_ACCESS_CTL);
	while(r & KSZ9896_REG_VLAN_TABLE_ACCESS_CTL_START)					{ r = ksz9896_read_reg(k, KSZ9896_REG_VLAN_TABLE_ACCESS_CTL); }
}
