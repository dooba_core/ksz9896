/* Dooba SDK
 * KSZ9896 Gigabit Ethernet Switch Driver
 */

#ifndef	__KSZ9896_H
#define	__KSZ9896_H

// External Includes
#include <stdint.h>
#include <avr/io.h>

// Ports
#define	KSZ9896_PORTS						6

// Commands
#define	KSZ9896_CMD_READ					0x60000000
#define	KSZ9896_CMD_WRITE					0x40000000

// Address Mask
#define	KSZ9896_ADDR_MASK					0x00ffffff

// Turn-Around Bits
#define	KSZ9896_TA_BITS						5

// Init Delay (ms)
#define	KSZ9896_DELAY_INIT					200

// I/O Delay (us)
#define	KSZ9896_DELAY_IO					1

// KSZ9896 Structure
struct ksz9896
{
	// I/O Configuration
	uint8_t cs_pin;
};

// Initialize
extern void ksz9896_init(struct ksz9896 *k, uint8_t cs_pin);

// Pre-Initialize
extern void ksz9896_preinit(struct ksz9896 *k, uint8_t cs_pin);

// Get Chip ID
extern uint32_t ksz9896_get_chip_id(struct ksz9896 *k);

// Enable Switch
extern void ksz9896_enable_switch(struct ksz9896 *k);

// Disable Switch
extern void ksz9896_disable_switch(struct ksz9896 *k);

// Read 8-bit Register
extern uint8_t ksz9896_read_reg(struct ksz9896 *k, uint16_t addr);

// Read 16-bit Register
extern uint16_t ksz9896_read_reg_s(struct ksz9896 *k, uint16_t addr);

// Read 32-bit Register
extern uint32_t ksz9896_read_reg_l(struct ksz9896 *k, uint16_t addr);

// Write 8-bit Register
extern void ksz9896_write_reg(struct ksz9896 *k, uint16_t addr, uint8_t val);

// Write 16-bit Register
extern void ksz9896_write_reg_s(struct ksz9896 *k, uint16_t addr, uint16_t val);

// Write 32-bit Register
extern void ksz9896_write_reg_l(struct ksz9896 *k, uint16_t addr, uint32_t val);

#endif
