/* Dooba SDK
 * KSZ9896 Gigabit Ethernet Switch Driver
 */

// Internal Includes
#include "regs.h"
#include "monitor.h"

// Get current monitor output ("sniffer") port
uint8_t ksz9896_monitor_get_out_port(struct ksz9896 *k)
{
	uint8_t i;

	// Get Sniffer Port
	for(i = 0; i < KSZ9896_PORTS; i = i + 1)
	{
		if(ksz9896_read_reg(k, KSZ9896_REG_PORT_MIRROR_CTL(i)) & KSZ9896_REG_PORT_MIRROR_CTL_SNIFFER_PORT)						{ return i; }
	}

	// None
	return KSZ9896_PORTS;
}

// Clear monitoring control
void ksz9896_monitor_clear(struct ksz9896 *k)
{
	uint8_t i;

	// Clear ports
	for(i = 0; i < KSZ9896_PORTS; i = i + 1)																					{ ksz9896_write_reg(k, KSZ9896_REG_PORT_MIRROR_CTL(i), 0); }
}

// Set monitor output ("sniffer") port
void ksz9896_monitor_set_out_port(struct ksz9896 *k, uint8_t port)
{
	// Enable Output Port
	ksz9896_monitor_set_out_port_s(k, port, 1);
}

// Disable monitor output ("sniffer") port
void ksz9896_monitor_set_out_port_off(struct ksz9896 *k, uint8_t port)
{
	// Disable Output Port
	ksz9896_monitor_set_out_port_s(k, port, 0);
}

// Configure monitor output ("sniffer") port
void ksz9896_monitor_set_out_port_s(struct ksz9896 *k, uint8_t port, uint8_t enable)
{
	uint8_t r;

	// Configure
	r = ksz9896_read_reg(k, KSZ9896_REG_PORT_MIRROR_CTL(port));
	if(enable)																													{ r = r | KSZ9896_REG_PORT_MIRROR_CTL_SNIFFER_PORT; }
	else																														{ r = r & ~KSZ9896_REG_PORT_MIRROR_CTL_SNIFFER_PORT; }
	ksz9896_write_reg(k, KSZ9896_REG_PORT_MIRROR_CTL(port), r);
}

// Clear monitor output ("sniffer") port
void ksz9896_monitor_out_port_clear(struct ksz9896 *k)
{
	uint8_t i;

	// Clear ports
	for(i = 0; i < KSZ9896_PORTS; i = i + 1)																					{ ksz9896_write_reg(k, KSZ9896_REG_PORT_MIRROR_CTL(i), ksz9896_read_reg(k, KSZ9896_REG_PORT_MIRROR_CTL(i)) & ~KSZ9896_REG_PORT_MIRROR_CTL_SNIFFER_PORT); }
}

// Sniff RX on port
void ksz9896_monitor_sniff_rx(struct ksz9896 *k, uint8_t port)
{
	// Enable
	ksz9896_monitor_set_sniff_rx(k, port, 1);
}

// Configure RX sniff on port
void ksz9896_monitor_set_sniff_rx(struct ksz9896 *k, uint8_t port, uint8_t enable)
{
	uint8_t r;

	// Configure
	r = ksz9896_read_reg(k, KSZ9896_REG_PORT_MIRROR_CTL(port));
	if(enable)																													{ r = r | KSZ9896_REG_PORT_MIRROR_CTL_RX_SNIFF; }
	else																														{ r = r & ~KSZ9896_REG_PORT_MIRROR_CTL_RX_SNIFF; }
	ksz9896_write_reg(k, KSZ9896_REG_PORT_MIRROR_CTL(port), r);
}

// Sniff TX on port
void ksz9896_monitor_sniff_tx(struct ksz9896 *k, uint8_t port)
{
	// Enable
	ksz9896_monitor_set_sniff_tx(k, port, 1);
}

// Configure TX sniff on port
void ksz9896_monitor_set_sniff_tx(struct ksz9896 *k, uint8_t port, uint8_t enable)
{
	uint8_t r;

	// Configure
	r = ksz9896_read_reg(k, KSZ9896_REG_PORT_MIRROR_CTL(port));
	if(enable)																													{ r = r | KSZ9896_REG_PORT_MIRROR_CTL_TX_SNIFF; }
	else																														{ r = r & ~KSZ9896_REG_PORT_MIRROR_CTL_TX_SNIFF; }
	ksz9896_write_reg(k, KSZ9896_REG_PORT_MIRROR_CTL(port), r);
}

// Clear sniff control on port
void ksz9896_monitor_sniff_clear(struct ksz9896 *k, uint8_t port)
{
	// Clear sniff control
	ksz9896_write_reg(k, KSZ9896_REG_PORT_MIRROR_CTL(port), ksz9896_read_reg(k, KSZ9896_REG_PORT_MIRROR_CTL(port)) & ~(KSZ9896_REG_PORT_MIRROR_CTL_RX_SNIFF | KSZ9896_REG_PORT_MIRROR_CTL_TX_SNIFF));
}

// Clear sniff control on all ports
void ksz9896_monitor_sniff_clear_all(struct ksz9896 *k)
{
	uint8_t i;

	// Clear ports
	for(i = 0; i < KSZ9896_PORTS; i = i + 1)																					{ ksz9896_write_reg(k, KSZ9896_REG_PORT_MIRROR_CTL(i), ksz9896_read_reg(k, KSZ9896_REG_PORT_MIRROR_CTL(i)) & ~(KSZ9896_REG_PORT_MIRROR_CTL_RX_SNIFF | KSZ9896_REG_PORT_MIRROR_CTL_TX_SNIFF)); }
}
