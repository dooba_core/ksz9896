/* Dooba SDK
 * KSZ9896 Gigabit Ethernet Switch Driver
 */

// External Includes
#include <util/delay.h>
#include <spi/spi.h>
#include <dio/dio.h>

// Internal Includes
#include "regs.h"
#include "ksz9896.h"

// Initialize
void ksz9896_init(struct ksz9896 *k, uint8_t cs_pin)
{
	// Pre-Init
	ksz9896_preinit(k, cs_pin);

	// Setup Structure
	k->cs_pin = cs_pin;

	// Delay
	_delay_ms(KSZ9896_DELAY_INIT);
}

// Pre-Initialize
void ksz9896_preinit(struct ksz9896 *k, uint8_t cs_pin)
{
	// Configure Pins
	dio_output(cs_pin);

	// Initialize Pins
	dio_hi(cs_pin);
}

// Get Chip ID
uint32_t ksz9896_get_chip_id(struct ksz9896 *k)
{
	// Read Long Register
	return ksz9896_read_reg_l(k, KSZ9896_REG_CHIP_ID);
}

// Enable Switch
void ksz9896_enable_switch(struct ksz9896 *k)
{
	uint8_t r;

	// Configure
	r = ksz9896_read_reg(k, KSZ9896_REG_SWITCH_OP);
	ksz9896_write_reg(k, KSZ9896_REG_SWITCH_OP, r | KSZ9896_REG_SWITCH_OP_STARTSWITCH);
}

// Disable Switch
void ksz9896_disable_switch(struct ksz9896 *k)
{
	uint8_t r;

	// Configure
	r = ksz9896_read_reg(k, KSZ9896_REG_SWITCH_OP);
	ksz9896_write_reg(k, KSZ9896_REG_SWITCH_OP, r & ~KSZ9896_REG_SWITCH_OP_STARTSWITCH);
}

// Read 8-bit Register
uint8_t ksz9896_read_reg(struct ksz9896 *k, uint16_t addr)
{
	uint32_t cmd;
	uint8_t cmd_b[sizeof(uint32_t)];
	uint8_t i;
	uint8_t r;

	// Prepare Command
	cmd = KSZ9896_CMD_READ | (((uint32_t)addr) << KSZ9896_TA_BITS);

	// Pull CS
	dio_lo(k->cs_pin);

	// Perform I/O Command
	for(i = 0; i < sizeof(uint32_t); i = i + 1)				{ cmd_b[i] = (cmd >> ((sizeof(uint32_t) - (i + 1)) * 8)) & 0x000000ff; }
	spi_tx(cmd_b, sizeof(uint32_t));
	spi_rx(&r, sizeof(uint8_t));

	// Release CS
	dio_hi(k->cs_pin);

	return r;
}

// Read 16-bit Register
uint16_t ksz9896_read_reg_s(struct ksz9896 *k, uint16_t addr)
{
	uint16_t r = 0;
	uint8_t i;

	// Loop
	for(i = 0; i < sizeof(uint16_t); i = i + 1)				{ r = (r << 8) | ksz9896_read_reg(k, addr + i); }

	return r;
}

// Read 32-bit Register
uint32_t ksz9896_read_reg_l(struct ksz9896 *k, uint16_t addr)
{
	uint32_t r = 0;
	uint8_t i;

	// Loop
	for(i = 0; i < sizeof(uint32_t); i = i + 1)				{ r = (r << 8) | ksz9896_read_reg(k, addr + i); }

	return r;
}

// Write 8-bit Register
void ksz9896_write_reg(struct ksz9896 *k, uint16_t addr, uint8_t val)
{
	uint32_t cmd;
	uint8_t cmd_b[sizeof(uint32_t)];
	uint8_t i;

	// Prepare Command
	cmd = KSZ9896_CMD_WRITE | (((uint32_t)addr) << KSZ9896_TA_BITS);

	// Pull CS
	dio_lo(k->cs_pin);

	// Perform I/O Command
	for(i = 0; i < sizeof(uint32_t); i = i + 1)				{ cmd_b[i] = (cmd >> ((sizeof(uint32_t) - (i + 1)) * 8)) & 0x000000ff; }
	spi_tx(cmd_b, sizeof(uint32_t));
	spi_tx(&val, sizeof(uint8_t));

	// Release CS
	dio_hi(k->cs_pin);
}

// Write 16-bit Register
void ksz9896_write_reg_s(struct ksz9896 *k, uint16_t addr, uint16_t val)
{
	uint8_t i;

	// Loop
	for(i = 0; i < sizeof(uint16_t); i = i + 1)				{ ksz9896_write_reg(k, addr + i, (val >> ((sizeof(uint16_t) - (i + 1)) * 8)) & 0x00ff); }
}

// Write 32-bit Register
void ksz9896_write_reg_l(struct ksz9896 *k, uint16_t addr, uint32_t val)
{
	uint8_t i;

	// Loop
	for(i = 0; i < sizeof(uint32_t); i = i + 1)				{ ksz9896_write_reg(k, addr + i, (val >> ((sizeof(uint32_t) - (i + 1)) * 8)) & 0x000000ff); }
}
